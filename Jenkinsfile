#!/usr/bin/env groovy

// we can override globally assigned library with a custom one:
// library identifier: 'jenkins-shared-library@main', retriever: modernSCM([$class: 'GitSCMSource', remote: <repo url of shared lib>, credentialsId: '<git creds>' ])
@Library('Aufgabe_Shared_Library')_

pipeline {
    agent any
    /*
    environment {
        NEW_VERSION = "1.3.0"
        BRANCH_NAME = "hii"
        SERVER_CREDENTIALS = credentials('nexus') // way to use credentials. first declare a variable here and then use it in scripts using ${SERVER_CREDENTIALS}
    }*/
    /*
    tools {
        maven "Maven"
    } this definition will make mvn command available in all the stages*/

    
    // parameters {
    //     string(name: "TaskID", defaultValue: '', description: 'Test')
    // } you can parametrize alsi with choice and booleanParam
    

    stages {
        // stage ("init") {
        //     steps {
        //         script {
        //             // gv = load "./infra/script.groovy"
        //             //loginDocker "compitofrontend.nexus.myroslav-symchych.space"
                    
        //         }
        //     }
        // }
        stage('Provisioning (init)') {
            environment {
                AWS_ACCESS_KEY_ID = credentials("jenkins_access_key_id")
                AWS_SECRET_ACCESS_KEY = credentials("jenkins_secret_access_key")
            }
            // when { branch pattern: "Learning-\\d+", comparator: "REGEXP" }
            steps {
                
                script {
                    sh "terraform init"
                }

            }
        }
        stage('Provisioning (apply)') {
            environment {
                AWS_ACCESS_KEY_ID = credentials("jenkins_access_key_id")
                AWS_SECRET_ACCESS_KEY = credentials("jenkins_secret_access_key")
            }
            steps {
                
                    script {
                        sh "terraform apply --auto-approve"
                    }

            }
        }
        stage('Creating kubeconfig') {
            environment {
                AWS_ACCESS_KEY_ID = credentials("jenkins_access_key_id")
                AWS_SECRET_ACCESS_KEY = credentials("jenkins_secret_access_key")
            }
            steps {               
                script {
                    CLUSTER_ENDPOINT = sh(
                        script: "terraform output cluster_endpoint",
                        returnStdout: true
                    ).trim()
                    CLUSTER_CERTIFICATE_AUTHORITY_DATA = sh(
                        script: "terraform output cluster_certificate_authority_data",
                        returnStdout: true
                    ).trim()
                    sh "cp kubeconfig_example config"
                    sh "sed -i 's#<endpoint-url>#${CLUSTER_ENDPOINT}#g; s#<base64-encoded-ca-cert>#${CLUSTER_CERTIFICATE_AUTHORITY_DATA}#g' config" 
                    sh "mv -f config /var/jenkins_home/.kube/" // aws eks update-kubeconfig --name example
                } 
            }
        }
        stage('Termination') {
            environment {
                AWS_ACCESS_KEY_ID = credentials("jenkins_access_key_id")
                AWS_SECRET_ACCESS_KEY = credentials("jenkins_secret_access_key")
            }
            // when { branch pattern: "Learning-\\d+", comparator: "REGEXP" }
            steps {                
                script {
                    def buildCoreImage = input(
                        message: 'Terminate cluster now?',
                        ok: 'Yes', 
                        parameters: [
                        booleanParam(defaultValue: true, description: 'Push the button to terminate cluster now.', name: 'Yes?')
                        ]
                        )
                    sh "terraform destroy --auto-approve"            
                }
            }
        }
        
        // stage('Increment version for dev task') {
        //     // when { branch pattern: "Learning-\\d+", comparator: "REGEXP" }
        //     steps {
        //         dir("client") {
        //             sh "npm version minor"
        //             script {
        //                 def matcher = readFile("package.json") =~ "\"version\": \"(.+)\","
        //                 def version = matcher[0][1]
        //                 env.IMAGE_NAME = "$version-$BUILD_NUMBER"
        //             }
        //         }
        //     }
        // }
        // stage('Increment version for dev task') {
        //     when {
        //         expression{
        //             BRANCH_NAME == 'DEV-*'
        //         }
        //     }
        // stage('Build frontend') {
        //     when {
        //         expression{
        //             BRANCH_NAME == "FE-1"
        //         }
        //     }
        //     steps {
        //         script {
        //             buildDockerImage 'compitofrontend.nexus.myroslav-symchych.space/frontend:$IMAGE_NAME', './frontend/'
        //             pushDockerImage 'compitofrontend.nexus.myroslav-symchych.space/frontend:$IMAGE_NAME'
        //         }
        //     }
        // }
                
        
        // stage('test') {
        //     steps {
        //         echo "test"
        //     } 
        // }
        // stage('Publish docker image') {
        //     when {
        //         expression{ 
        //             BRANCH_NAME == 'Learning-jenkinsfile-syntax'
        //         }
        //     }
        //     steps {
        //         script {
        //             loginDocker 'nexus.myroslav-symchych.space:2101'
        //             loginDocker 'nexus.myroslav-symchych.space:2102'
        //             pushFrontendDockerImage 'nexus.myroslav-symchych.space:2102/testfront:1.1'
        //             pushBackendDockerImage 'nexus.myroslav-symchych.space:2101/testback:1.1'
        //         }
        //     } 
        // }
        // stage('Publish images') {
        //     /*when {
        //         expression {
        //             BRANCH_NAME == 'dev' || BRANCH_NAME == 'master' // will only be executed if X or X, otherwise skipped. For AND - &&
        //             params.TaskID == true // same but for parameters
        //         }
        //     } */
        //     steps {
        //         echo 'Testing..'
        //     }
        // }
        // stage('Deploy') {
        //     input {
        //         message "Select the environment to deploy to"
        //         ok "Env selected"
        //         parameters {
        //             choice(name: 'environment', choices: ["dev", "stage", "prod"], description: 'choose please') // you can specify several choices as well. In the input specify the needed fields, and in the step specify what you will do with those steps. If you want to assign variables in the script, you have to place them there, like this: 
        //             /*script {
        //                 env.ENVIRONMENT_SELECTION = input message: "Select the environment to deploy to", ok: "Done", parameters: [choice(name: 'ONE', choices: ["one", "two", "three"], description: '')]
        //                 ]
        //             }*/
        //         }
        //     }
        //     steps {
        //         echo 'Deploying....'
        //         echo "deploying with ${SERVER_CREDENTIALS}"
        //         echo 'wrap'
        //         /*withCredentials([usernamePassword(credentials: 'test', usernameVariable: 'USER', passwordVariable: 'PWD')]){
        //             sh "${USER} ${PWD}"
        //         }*/ 
        //         //supposed to give username and password
        //         echo "Deploying to ${environment}"
        //     }
        // }
        // stage("Push version to git repo") {
        //     steps{
        //         script {
        //             pushNpmVersion "gitlab.com/myroslav-symchych-repos/aufgabe-app.git", "$BRANCH_NAME", "./infra/versioning/version.json"
        //         }
        //     }
        // }
    }
}

