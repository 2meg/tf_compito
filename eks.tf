provider "kubernetes" {
    host = data.aws_eks_cluster.compito_cluster.endpoint
    token = data.aws_eks_cluster_auth.compito_cluster.token
    cluster_ca_certificate = base64decode(data.aws_eks_cluster.compito_cluster.certificate_authority.0.data)
}


module "eks" {
    source = "terraform-aws-modules/eks/aws"
    version = "17.23.0"
    
    cluster_name = var.cluster_name
    cluster_version = "1.21"
    subnets = module.compito_vpc.private_subnets
    vpc_id = module.compito_vpc.vpc_id
    tags = {
        environment = "development"
        application = "compito"
    }

    node_groups  = [{
        cluster_name = var.cluster_name
        desired_capacity = var.desired_capacity
        min_capacity     = var.min_capacity
        max_capacity     = var.max_capacity
        iam_role_arn = aws_iam_role.nodegrouprole.arn
        instance_types = var.instance_type
        name = var.nodegroup_name
        subnets = module.compito_vpc.private_subnets
        }]        
}
