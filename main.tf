provider "aws" {
    region = "eu-central-1"
}

terraform {
    required_version = ">=0.12"
    backend "s3" {
        bucket = "aufgabe-tf-s3"
        key = "compito/state.tfstate"
        region = "eu-central-1"
    }
}

module "compito_vpc" {
    source = "terraform-aws-modules/vpc/aws"
    version = "3.11.0"
    name = "compito_vpc"
    cidr = var.vpc_cidr_block
    private_subnets = var.private_subnet_cidr_blocks
    public_subnets = var.public_subnet_cidr_blocks
    azs = data.aws_availability_zones.available.names 
    
    enable_nat_gateway = true
    single_nat_gateway = true # route all traffic to a single nat gateway
    enable_dns_hostnames = true # enable dns names on ec2 instances

    tags = {
        "kubernetes.io/cluster/compito-eks-cluster" = "shared"
    }

    public_subnet_tags = {
        "kubernetes.io/cluster/compito_cluster" = "shared"
        "kubernetes.io/role/elb" = 1 # eks (ccm) and aws elb controller needs to know where to provision those elbs 
    }

    private_subnet_tags = {
        "kubernetes.io/cluster/compito_cluster" = "shared" # basically tells cloud controller manager that this vpc is intended for eks use
        "kubernetes.io/role/internal-elb" = 1 
    }

}


