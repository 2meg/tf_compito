vpc_cidr_block = "10.0.0.0/16"
private_subnet_cidr_blocks = ["10.0.1.0/24", "10.0.2.0/24"] #, "10.0.2.0/24"
public_subnet_cidr_blocks = ["10.0.101.0/24", "10.0.102.0/24"] # , "10.0.102.0/24"
desired_capacity = 2
min_capacity     = 2
max_capacity     = 3

instance_type = ["t2.small"]
nodegroup_name = "compito_nodegroup"
cluster_name = "compito_cluster"

env_prefix = "prod"